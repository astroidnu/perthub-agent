package com.scoproject.pertahubagent.customcomponent

import android.content.Context
import android.graphics.Color
import android.view.View
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import android.graphics.Typeface
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.ImageView
import com.scoproject.pertahubagent.R
import android.widget.LinearLayout




/**
 * Created by ibnumuzzakkir on 15/04/18.
 * Android Engineer
 * SCO Project
 */
class CustomTextSliderView(context: Context) : BaseSliderView(context) {
    override fun getView(): View {
        val v = LayoutInflater.from(context).inflate(R.layout.render_type_text, null)
        val target = v.findViewById(R.id.daimajia_slider_image) as ImageView
        val description = v.findViewById(R.id.description) as TextView

        val frame = v.findViewById(R.id.description_layout) as LinearLayout
        frame.setBackgroundColor(Color.TRANSPARENT)

        description.text = getDescription()
        bindEventAndShow(v, target)
        return v
    }

}