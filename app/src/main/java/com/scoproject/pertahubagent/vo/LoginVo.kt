package com.scoproject.pertahubagent.vo

import com.google.gson.annotations.SerializedName

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */
data class LoginVo(
        @SerializedName("token") val token :String,
        @SerializedName("id") val id :Int,
        @SerializedName("group_name") val groupName :String
)