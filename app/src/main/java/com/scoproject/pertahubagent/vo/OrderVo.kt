package com.scoproject.pertahubagent.vo

import com.google.gson.annotations.SerializedName

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */
data class OrderVo (
    @SerializedName("order_id") val orderId :String,
    @SerializedName("create_date") val createdDate :String,
    @SerializedName("product_name") val productName :String,
    @SerializedName("price") val price :String,
    @SerializedName("qty") val qty :String,
    @SerializedName("amount") val amount :String,
    @SerializedName("request_by") val requestBy :String,
    @SerializedName("processed_by") val processedBy :String,
    @SerializedName("status") val status :String
)