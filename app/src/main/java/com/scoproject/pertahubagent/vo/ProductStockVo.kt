package com.scoproject.pertahubagent.vo

import com.google.gson.annotations.SerializedName

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */

data class ProductStockVo(
        @SerializedName("id") val id :String,
        @SerializedName("product_name") val productName :String,
        @SerializedName("category_name") val categoryName: String,
        @SerializedName("stock") val stock: String,
        @SerializedName("qty") val qty :String,
        @SerializedName("my_stock") val myStock :String
)
