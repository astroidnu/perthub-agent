package com.scoproject.pertahubagent.vo

import com.google.gson.annotations.SerializedName

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */
data class ProfileVo(
        @SerializedName("id") val id :String,
        @SerializedName("name") val name :String,
        @SerializedName("username") val username :String,
        @SerializedName("email") val email :String,
        @SerializedName("latit") val latit :String,
        @SerializedName("longit") val longit :String,
        @SerializedName("alamat") val alamat :String,
        @SerializedName("no_hp") val noHp :String,
        @SerializedName("foto") val foto :String
)