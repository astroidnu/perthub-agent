package com.scoproject.pertahubagent.vo

import com.google.gson.annotations.SerializedName

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */
data class CategoryVo(
        @SerializedName("id") val id :String,
        @SerializedName("category_name") val categoryName :String,
        @SerializedName("image_src") val imageSrc :String
)