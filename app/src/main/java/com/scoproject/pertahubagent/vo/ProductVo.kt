package com.scoproject.pertahubagent.vo

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */
class ProductVo() : Parcelable {
    @SerializedName("id") var id :String? = null
    @SerializedName("product_name") var categoryName :String? = null
    @SerializedName("image_src") var imageSrc :String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        categoryName = parcel.readString()
        imageSrc = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(categoryName)
        parcel.writeString(imageSrc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductVo> {
        override fun createFromParcel(parcel: Parcel): ProductVo {
            return ProductVo(parcel)
        }

        override fun newArray(size: Int): Array<ProductVo?> {
            return arrayOfNulls(size)
        }
    }
}