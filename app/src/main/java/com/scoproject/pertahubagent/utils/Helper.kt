package com.scoproject.pertahubagent.utils

import android.content.Context
import android.net.ConnectivityManager
import com.scoproject.pertahubagent.R
import retrofit2.HttpException


/**
 * Created by ibnumuzzakkir on 27/01/18.
 * Android Engineer
 * SCO Project
 */
class Helper{

    /**
     * Checking Internet Connection
     * */

    fun isConnectingToInternet(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    /**
     * Handling error response Code
     * */

    fun getErrorResponseCode(e:Throwable) :Int {
        return (e as HttpException).code()
    }

    fun getOrderColorCode(orderStatus : Int) : Int {
        return when(orderStatus){
            0 -> R.color.colorPrimary
            1 -> R.color.colorGreen
            2 -> R.color.colorRed
            3 -> R.color.colorGreen
            else -> {
                R.color.colorPrimary
            }
        }
    }




}