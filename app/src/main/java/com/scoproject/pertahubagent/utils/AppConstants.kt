package com.scoproject.pertahubagent.utils

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */
class AppConstants {
    interface HTTP_RESPONSE_CODE {
        companion object {
            const val UNAUTHORIZED = 401
        }
    }

    interface JSON_RESPONSE_CODE{
        companion object {
            const val OK = 0
        }
    }

    interface ORDER_STATUS {
        companion object {
            const val REQUESTED = "Meminta"
            const val CANCEL = "Batal"
            const val APPROVED = "Disetujui"
            const val DONE = "Selesai"
        }
    }
}
