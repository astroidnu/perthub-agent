package com.scoproject.pertahubagent.di.module.builder

import com.scoproject.pertahubagent.ui.fragment.history.HistoryFragment
import com.scoproject.pertahubagent.ui.fragment.history.completed.CompletedFragment
import com.scoproject.pertahubagent.ui.fragment.history.inprocess.InProcessFragment
import com.scoproject.pertahubagent.ui.fragment.history.waitingforapprove.WaitingForApproveFragment
import com.scoproject.pertahubagent.ui.fragment.home.HomeFragment
import com.scoproject.pertahubagent.ui.fragment.profile.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
@Module
abstract class DashboardFragmentBuilder {
    @ContributesAndroidInjector
    internal abstract fun contributeHistoryFragment(): HistoryFragment

    @ContributesAndroidInjector
    internal abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCompletedFragment(): CompletedFragment

    @ContributesAndroidInjector
    internal abstract fun contributeInProcessFragment(): InProcessFragment

    @ContributesAndroidInjector
    internal abstract fun contributeWaitingApproveFragment(): WaitingForApproveFragment
}