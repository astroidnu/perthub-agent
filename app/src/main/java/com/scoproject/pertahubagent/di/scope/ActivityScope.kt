package com.scoproject.pertahubagent.di.scope

import javax.inject.Scope

/**
 * Created by ibnumuzzakkir on 12/04/18.
 * Android Engineer
 * SCO Project
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope