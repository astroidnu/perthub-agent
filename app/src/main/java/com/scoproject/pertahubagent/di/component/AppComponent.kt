package com.scoproject.pertahubagent.di.component

import android.app.Application
import com.scoproject.pertahubagent.PertahubAgentApp
import com.scoproject.pertahubagent.di.module.AppModule
import com.scoproject.pertahubagent.di.module.NetworkModule
import com.scoproject.pertahubagent.di.module.builder.ActivityBuilder
import com.scoproject.pertahubagent.service.MyFirebaseMessagingService
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Created by ibnumuzzakkir on 12/04/18.
 * Android Engineer
 * SCO Project
 */
@Singleton
@Component(modules = [(AndroidInjectionModule::class),
    (AppModule::class),
    (ActivityBuilder::class),
    (NetworkModule::class)])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun networkModule(networkModule: NetworkModule): Builder
        fun build(): AppComponent
    }

    fun inject(app: PertahubAgentApp)

    fun inject(myFirebaseMessagingService: MyFirebaseMessagingService)

}