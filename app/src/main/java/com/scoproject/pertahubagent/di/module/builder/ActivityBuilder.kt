package com.scoproject.pertahubagent.di.module.builder

import com.scoproject.pertahubagent.di.scope.ActivityScope
import com.scoproject.pertahubagent.ui.activity.dashboard.DashboardActivity
import com.scoproject.pertahubagent.ui.activity.dashboard.DashboardModule
import com.scoproject.pertahubagent.ui.activity.login.LoginActivity
import com.scoproject.pertahubagent.ui.activity.login.LoginModule
import com.scoproject.pertahubagent.ui.activity.productdetail.ProductDetailActivity
import com.scoproject.pertahubagent.ui.activity.productdetail.ProductDetailModule
import com.scoproject.pertahubagent.ui.activity.productlist.ProductListActivity
import com.scoproject.pertahubagent.ui.activity.productlist.ProductListModule
import com.scoproject.pertahubagent.ui.activity.register.RegisterActivity
import com.scoproject.pertahubagent.ui.activity.register.RegisterModule
import com.scoproject.pertahubagent.ui.activity.responseorderpage.ResponseOrderPageActivity
import com.scoproject.pertahubagent.ui.activity.responseorderpage.ResponseOrderPageModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
@Module
abstract class ActivityBuilder{
    @ActivityScope
    @ContributesAndroidInjector(modules = [(LoginModule::class)])
    internal abstract fun bindLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(RegisterModule::class)])
    internal abstract fun bindRegisterActivity(): RegisterActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(DashboardModule::class),
        (DashboardFragmentBuilder::class)])
    internal abstract fun bindDashboardActivity(): DashboardActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(ProductListModule::class)])
    internal abstract fun bindProductListActivity(): ProductListActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(ProductDetailModule::class)])
    internal abstract fun bindProductDetailActivity (): ProductDetailActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(ResponseOrderPageModule::class)])
    internal abstract fun bindResponseOrderPageActivity(): ResponseOrderPageActivity
}