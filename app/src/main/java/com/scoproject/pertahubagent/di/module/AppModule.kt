package com.scoproject.pertahubagent.di.module

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.utils.AppSchedulerProvider
import com.scoproject.pertahubagent.utils.Helper
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

/**
 * Created by ibnumuzzakkir on 12/04/18.
 * Android Engineer
 * SCO Project
 */
@Module
class AppModule{
    @Provides @Singleton
    internal fun provideContext(application: Application): Context {
        return application
    }

    @Provides @Singleton
    fun provideCompositeDisposable() = CompositeDisposable()

    @Provides @Singleton
    internal fun gson(): Gson = Gson()

    @Provides @Singleton
    fun provideSchedulerProvider() = AppSchedulerProvider()

    @Singleton @Provides
    fun provideLoginSession() =  LoginSession()

    @Singleton @Provides
    fun provideHelper() =  Helper()

}