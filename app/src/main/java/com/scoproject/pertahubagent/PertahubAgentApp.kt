package com.scoproject.pertahubagent

import android.app.Activity
import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.scoproject.pertahubagent.di.component.AppComponent
import com.scoproject.pertahubagent.di.component.DaggerAppComponent
import com.scoproject.pertahubagent.di.module.NetworkModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 12/04/18.
 * Android Engineer
 * SCO Project
 */
class PertahubAgentApp : MultiDexApplication(), HasActivityInjector {
    companion object {
        @JvmStatic lateinit var instance: PertahubAgentApp
        @JvmStatic lateinit var appComponent: AppComponent
    }

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
    override fun onCreate() {
        super.onCreate()

        //Set Instance
        instance = this
        //Create App Component
        appComponent = createComponent()
        appComponent.inject(this)


    }

    private fun createComponent(): AppComponent {
        return DaggerAppComponent.builder()
                .application(this)
                .networkModule(NetworkModule(this))
                .build()
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

}