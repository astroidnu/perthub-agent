package com.scoproject.pertahubagent.ui.common.navigationcontroller

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import com.scoproject.pertahubagent.ui.fragment.history.HistoryFragment
import com.scoproject.pertahubagent.ui.fragment.home.HomeFragment
import com.scoproject.pertahubagent.ui.fragment.profile.ProfileFragment

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
class FragmentNavigation(activity: AppCompatActivity, containerId : Int) : BaseFragmentNavigation(activity,containerId) {
    /**
     * Navigate To History Page
     * */

    fun navigateToHistoryPage() {
        val mHistoryFragment = HistoryFragment()
        loadFragment(mHistoryFragment, "HistoryFragment")
    }

    /**
     * Navigate To Home Fragment
     * */

    fun navigateToHomePage() {
        val mHomeFragment = HomeFragment()
        loadFragment(mHomeFragment, "HomeFragment")
    }

    /**
     * Navigate To Profile Fragment
     * */

    fun navigateToProfilePage() {
        val mProfilePage = ProfileFragment()
        loadFragment(mProfilePage, "ProfileFragment")
    }
}