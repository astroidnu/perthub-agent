package com.scoproject.pertahubagent.ui.common.navigationcontroller

import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

/**
 * Created by ibnumuzzakkir on 12/04/18.
 * Android Engineer
 * SCO Project
 */
open class BaseFragmentNavigation(val activity: AppCompatActivity, private val containerId: Int) {


    /**
     * assign Fragment Manager from current activity
     * */

    private var mFragmentManager = activity.supportFragmentManager

    /**
     * Load Fragment
     * Handling all load fragment navigation
     * */
    fun loadFragment(fragment: Fragment?, tag :String) {
        mFragmentManager.beginTransaction()
                .remove(fragment)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    /**
     * Load dialog fragment
     * Handling all load dialog fragment navigation
     * */

    fun loadDialogFragment(mDialogFragment: DialogFragment, mTagFragment: String) {
        mDialogFragment.show(mFragmentManager, mTagFragment)
    }

    /**
     * Get open current fragment
     * */

    fun getOpenFragment(): Fragment {
        return mFragmentManager.findFragmentById(containerId)
    }

}