package com.scoproject.pertahubagent.ui.fragment.history.inprocess

import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.BasePresenter
import com.scoproject.pertahubagent.ui.fragment.history.HistoryContract
import com.scoproject.pertahubagent.utils.SchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 16/04/18.
 * Android Engineer
 * SCO Project
 */
class InProcessPresenter @Inject constructor(private val mNetworkService: NetworkService,
                                             private val mLoginSession: LoginSession,
                                             disposable : CompositeDisposable,
                                             scheduler : SchedulerProvider
):  BasePresenter<InProcessContract.View>(disposable,scheduler), InProcessContract.UserActionListener {

    override fun getHistoryOrder() {
        view?.showLoading()
        disposable.add(
                mNetworkService.getAllHistoryOrder("bearer " + mLoginSession.getLoginToken())
                        .subscribeOn(scheduler.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            result ->
                            view?.hideLoading()
                            val data = result.data.filter { it.status == 0 }.toList()
                            if(data.isNotEmpty()){
                                view?.hideNoHistory()
                                view?.setAdapter(data)
                            }else{
                                view?.showNoHistory()
                            }
                        },{ e ->
                            view?.hideLoading()
                        })
        )
    }

}