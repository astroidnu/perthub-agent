package com.scoproject.pertahubagent.ui.fragment.history

import com.scoproject.pertahubagent.ui.base.BaseView

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
class HistoryContract {
    interface View : BaseView {
        fun setupUI()

    }

    interface UserActionListener {

    }
}