package com.scoproject.pertahubagent.ui.activity.productdetail

import com.google.gson.Gson
import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.BasePresenter
import com.scoproject.pertahubagent.utils.Helper
import com.scoproject.pertahubagent.utils.SchedulerProvider
import com.scoproject.pertahubagent.vo.ProductStockVo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 20/04/18.
 * Android Engineer
 * SCO Project
 */
class ProductDetailPresenter  @Inject constructor(private val mNetworkService: NetworkService,
                                                  private val mHelper: Helper,
                                                  private val mGson : Gson,
                                                  private val mLoginSession : LoginSession,
                                                  disposable : CompositeDisposable,
                                                  scheduler : SchedulerProvider
):  BasePresenter<ProductDetailContract.View>(disposable,scheduler),ProductDetailContract.UserActionListener {


    lateinit var mProductVo : ProductStockVo
    override fun getProduct(productId :String) {
        view?.showLoading()
        disposable.add(
                mNetworkService.getProductStock("bearer " + mLoginSession.getLoginToken(),productId)
                        .subscribeOn(scheduler.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            result ->
                            view?.hideLoading()
                            mProductVo = result.data[0]
                            view?.setContent(mProductVo.productName, mProductVo.qty, mProductVo.myStock)
                        },{ e ->
                            view?.hideLoading()
                            view?.showToast(e.message.toString())
                        })
        )
    }

    override fun submitOrder(amountOrder: String?, productId: String?) {
        if(amountOrder != null && productId != null){
            val currentAgentStock = mProductVo.qty
            if(amountOrder.isNotEmpty()){
                if(currentAgentStock.toInt() < amountOrder.toInt()){
                    view?.showToast("Periksa kembali jumlah barang yang Anda order!")
                }else{
                    view?.showLoading()
                    val data = HashMap<String, String>()
                    data["product_id"] = productId
                    data["qty"] = amountOrder
                    disposable.add(
                            mNetworkService.postOrder("bearer " + mLoginSession.getLoginToken(),data)
                                    .subscribeOn(scheduler.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe ({
                                        result ->
                                        view?.hideLoading()
                                        view?.navigateToSuccessOrderPage()
                                    },{ e ->
                                        view?.hideLoading()
                                        view?.showToast(e.message.toString())
                                    })
                    )
                }
            }else{
                view?.showToast("Inputan tidak boleh kosong")
            }
        }else {
            view?.showToast("Terjadi kesalahan, tidak dapat memproses order")
        }

    }

}