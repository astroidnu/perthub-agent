package com.scoproject.pertahubagent.ui.fragment.history.waitingforapprove

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.adapter.setUp
import com.scoproject.pertahubagent.ui.common.BaseFragment
import com.scoproject.pertahubagent.utils.AppConstants
import com.scoproject.pertahubagent.vo.HistoryOrderVo
import kotlinx.android.synthetic.main.fragment_inprocess_completed.*
import kotlinx.android.synthetic.main.item_history_content.view.*
import javax.inject.Inject
import android.content.DialogInterface
import android.content.DialogInterface.BUTTON_NEUTRAL
import android.support.v7.app.AlertDialog
import com.scoproject.pertahubagent.utils.Helper


/**
 * Created by ibnumuzzakkir on 22/04/18.
 * Android Engineer
 * SCO Project
 */
class WaitingForApproveFragment : BaseFragment(), WaitingForApproveContract.View {
    @Inject
    lateinit var mWaitingForApprovePresenter: WaitingForApprovePresenter

    @Inject
    lateinit var mHelper : Helper

    override fun getLayoutId(): Int {
        return R.layout.fragment_inprocess_completed
    }

    override fun onLoadFragment(saveInstance: Bundle?) {
        mWaitingForApprovePresenter.attachView(this)
        mWaitingForApprovePresenter.getHistoryOrder()
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showToast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun setAdapter(data: List<HistoryOrderVo>) {
        rvInProcessCompleted.setUp(data, R.layout.item_history, {
            tvOrderId.text = it.orderId
            tvOrderAmount.text = it.qty
            tvOrderProductName.text = it.productName
            llHistorySendContent.setBackgroundColor(resources.getColor(mHelper.getOrderColorCode(it.status)))
            tvOrderStatus.text = when(it.status){
                0 -> AppConstants.ORDER_STATUS.REQUESTED
                1 -> AppConstants.ORDER_STATUS.APPROVED
                2 -> AppConstants.ORDER_STATUS.CANCEL
                3 -> AppConstants.ORDER_STATUS.DONE
                else -> {
                    "-"
                }
            }
        },{ data -> showAlertDialog(data.orderId)}, LinearLayoutManager(context))
    }

    override fun showNoHistory() {
        tvInProcessCompleteNoData.visibility = View.VISIBLE
        rvInProcessCompleted.visibility = View.GONE
    }

    override fun hideNoHistory() {
        tvInProcessCompleteNoData.visibility = View.GONE
        rvInProcessCompleted.visibility = View.VISIBLE
    }

    override fun showAlertDialog(orderId: String) {
        context?.let {
            val alertDialog = AlertDialog.Builder(it).create()
            alertDialog.setTitle("Konfirmasi")
            alertDialog.setMessage("Apakah Anda akan mengubah status permintaan order dengan id  = $orderId")
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "TUTUP",
                    { dialog, which -> dialog.dismiss() })
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "KONFIRMASI",
                    { dialog, which -> mWaitingForApprovePresenter.approveOrderStatus(orderId) })
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "BATAL",
                    { dialog, which -> mWaitingForApprovePresenter.cancelOrderStatus(orderId) })
            alertDialog.show()
        }
    }

    override fun navigateToDashboard() {
        mActivityNavigation.navigateToDashboardPage()
    }

    override fun onStop() {
        super.onStop()
        mWaitingForApprovePresenter.detachView()
    }


}