package com.scoproject.pertahubagent.ui.fragment.home

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.daimajia.slider.library.Indicators.PagerIndicator
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.adapter.setUp
import com.scoproject.pertahubagent.customcomponent.CustomTextSliderView
import com.scoproject.pertahubagent.ui.activity.dashboard.DashboardActivity
import com.scoproject.pertahubagent.ui.common.BaseFragment
import com.scoproject.pertahubagent.vo.CategoryVo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.item_sell_product.view.*
import java.util.*
import javax.inject.Inject


/**
 * Created by ibnumuzzakkir on 15/04/18.
 * Android Engineer
 * SCO Project
 */
class HomeFragment : BaseFragment(), HomeContract.View{

    @Inject
    lateinit var mHomePresenter: HomePresenter

    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun onLoadFragment(saveInstance: Bundle?) {
        mHomePresenter.attachView(this)
        setupSlider()
        setupUI()
    }

    private fun setupSlider() {
        val data = HashMap<String, Int>()
        data["Hannibal"] = R.drawable.banner
        data["Big Bang Theory"] = R.drawable.banner

        for (name in data.keys) {
            val textSliderView = CustomTextSliderView(this.context!!)
            // initialize a SliderLayout
            textSliderView
                    .image(data[name]!!)
                    .scaleType = BaseSliderView.ScaleType.Fit

            //add your extra information
            textSliderView.bundle(Bundle())
            textSliderView.bundle
                    .putString("extra", name)

            sliderHome.addSlider(textSliderView)
        }

        sliderHome.setPresetTransformer(SliderLayout.Transformer.Default)
        sliderHome.indicatorVisibility = PagerIndicator.IndicatorVisibility.Visible
        sliderHome.setCustomIndicator(custom_indicator)
        sliderHome.setDuration(4000)

        mHomePresenter.getProduct()
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showError(error: String) {
    }

    override fun setUpProductList(data: List<CategoryVo>) {
        rvHomeSellProduct.setUp(data, R.layout.item_sell_product, {
            tvProductName.text = it.categoryName
            Picasso.with(context).load(it.imageSrc).into(ivProduct)

        },{ data -> mActivityNavigation.navigateToProductList(data.categoryName,data.id)}, GridLayoutManager(context,2))
    }

    override fun onStop() {
        super.onStop()
        mHomePresenter.detachView()
    }

    override fun setupUI() {
        if(activity is DashboardActivity){
            (activity as DashboardActivity).showToolbar()
        }
    }


}