package com.scoproject.pertahubagent.ui.fragment.history.inprocess

import com.scoproject.pertahubagent.ui.base.BaseView
import com.scoproject.pertahubagent.vo.HistoryOrderVo

/**
 * Created by ibnumuzzakkir on 16/04/18.
 * Android Engineer
 * SCO Project
 */
class InProcessContract {
    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun showNoHistory()
        fun hideNoHistory()
        fun showToast(msg :String)
        fun setAdapter(data :List<HistoryOrderVo>)
    }

    interface  UserActionListener {
        fun getHistoryOrder()
    }
}