package com.scoproject.pertahubagent.ui.activity.login

import com.google.gson.Gson
import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.BasePresenter
import com.scoproject.pertahubagent.utils.Helper
import com.scoproject.pertahubagent.utils.SchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
class LoginPresenter @Inject constructor(private val mNetworkService: NetworkService,
                                         private val mHelper: Helper,
                                         private val mGson :Gson,
                                         private val mLoginSession : LoginSession,
                                         disposable : CompositeDisposable,
                                         scheduler : SchedulerProvider
):  BasePresenter<LoginContract.View>(disposable,scheduler),LoginContract.UserActionListener {

    override fun checkLogin() {
        if(mLoginSession.getLoginToken().isNotEmpty()){
            view?.navigateToDashboard()
        }
    }

    override fun doLogin(username: String, password: String, fcmToken :String) {
        view?.showLoading()
        disposable.add(
                mNetworkService.login(username,password, fcmToken)
                        .subscribeOn(scheduler.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            result ->
                            view?.hideLoading()
                            mLoginSession.saveToken(result.data.token)
                            mLoginSession.saveUserGroupId(result.data.id)
                            mLoginSession.saveUserGroupName(result.data.groupName)
                            view?.navigateToDashboard()

                        },{ e ->
                            view?.hideLoading()
                            view?.showError("Login gagal!")
                        })
        )
    }

}