package com.scoproject.pertahubagent.ui.activity.responseorderpage

import android.os.Bundle
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.ui.common.BaseActivity
import kotlinx.android.synthetic.main.activity_response_order_page.*
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 21/04/18.
 * Android Engineer
 * SCO Project
 */
class ResponseOrderPageActivity : BaseActivity(), ResponseOrderPageContract.View {
    @Inject
    lateinit var mResponseOrderPagePresenter: ResponseOrderPagePresenter

    override fun onActivityReady(savedInstanceState: Bundle?) {
        mResponseOrderPagePresenter.attachView(this)
        setupUIListener()
    }

    override fun getLayoutId(): Int {
       return R.layout.activity_response_order_page
    }

    override fun setupUIListener() {
        btnResponseContinue.setOnClickListener {
            mActivityNavigation.navigateToDashboardPage()
        }
    }

}