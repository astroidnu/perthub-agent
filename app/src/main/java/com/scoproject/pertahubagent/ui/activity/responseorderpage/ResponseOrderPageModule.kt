package com.scoproject.pertahubagent.ui.activity.responseorderpage

import com.google.gson.Gson
import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.di.scope.ActivityScope
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.activity.productlist.ProductListActivity
import com.scoproject.pertahubagent.ui.activity.productlist.ProductListContract
import com.scoproject.pertahubagent.ui.activity.productlist.ProductListPresenter
import com.scoproject.pertahubagent.ui.common.navigationcontroller.ActivityNavigation
import com.scoproject.pertahubagent.utils.AppSchedulerProvider
import com.scoproject.pertahubagent.utils.Helper
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by ibnumuzzakkir on 21/04/18.
 * Android Engineer
 * SCO Project
 */
@Module
class ResponseOrderPageModule {
    @Provides
    @ActivityScope
    internal fun provideActivity(activity: ResponseOrderPageActivity): ResponseOrderPageContract.View {
        return activity
    }

    @Provides
    @ActivityScope
    internal fun provideActivityNavigation(activity: ResponseOrderPageActivity): ActivityNavigation {
        return ActivityNavigation(activity)
    }

    @Provides
    @ActivityScope
    internal fun providePresenter(compositeDisposable: CompositeDisposable, schedulerProvider: AppSchedulerProvider) : ResponseOrderPagePresenter {
        return ResponseOrderPagePresenter(compositeDisposable, schedulerProvider)
    }
}