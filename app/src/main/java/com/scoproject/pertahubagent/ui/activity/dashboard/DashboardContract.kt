package com.scoproject.pertahubagent.ui.activity.dashboard

/**
 * Created by ibnumuzzakkir on 15/04/18.
 * Android Engineer
 * SCO Project
 */
class DashboardContract {
    interface View {
        fun setupBottomNavigation()
        fun hideToolbar()
        fun showToolbar()
    }
}