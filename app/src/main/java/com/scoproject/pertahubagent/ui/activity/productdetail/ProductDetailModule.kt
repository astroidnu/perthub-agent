package com.scoproject.pertahubagent.ui.activity.productdetail

import com.google.gson.Gson
import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.di.scope.ActivityScope
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.activity.login.LoginPresenter
import com.scoproject.pertahubagent.ui.common.navigationcontroller.ActivityNavigation
import com.scoproject.pertahubagent.utils.AppSchedulerProvider
import com.scoproject.pertahubagent.utils.Helper
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by ibnumuzzakkir on 20/04/18.
 * Android Engineer
 * SCO Project
 */
@Module
class ProductDetailModule {
    @Provides
    @ActivityScope
    internal fun provideActivity(activity: ProductDetailActivity): ProductDetailContract.View {
        return activity
    }

    @Provides
    @ActivityScope
    internal fun provideActivityNavigation(activity: ProductDetailActivity): ActivityNavigation{
        return ActivityNavigation(activity)
    }

    @Provides
    @ActivityScope
    internal fun providePresenter(networkService: NetworkService,
                                  helper: Helper,
                                  gson : Gson,
                                  loginSession : LoginSession,
                                  compositeDisposable: CompositeDisposable,
                                  schedulerProvider: AppSchedulerProvider) : ProductDetailPresenter {
        return ProductDetailPresenter(networkService,helper,gson,loginSession,compositeDisposable, schedulerProvider)
    }
}