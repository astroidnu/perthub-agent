package com.scoproject.pertahubagent.ui.fragment.home

import android.util.Log
import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.BasePresenter
import com.scoproject.pertahubagent.utils.SchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 15/04/18.
 * Android Engineer
 * SCO Project
 */
class HomePresenter @Inject constructor(private val mNetworkService: NetworkService,
                                        private val mLoginSession : LoginSession,
                                        disposable : CompositeDisposable,
                                        scheduler : SchedulerProvider
):  BasePresenter<HomeContract.View>(disposable,scheduler),HomeContract.UserActionListener {

    override fun getProduct() {
        view?.showLoading()
        Log.d(javaClass.name, mLoginSession.getLoginToken())
        disposable.add(
                mNetworkService.getCategory("bearer " + mLoginSession.getLoginToken())
                        .subscribeOn(scheduler.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            result ->
                            view?.hideLoading()
                            view?.setUpProductList(result.data)
                        },{ e ->
                            view?.hideLoading()
                            view?.showError("Login gagal!")
                        })
        )
    }

}