package com.scoproject.pertahubagent.ui.activity.productlist

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.adapter.setUp
import com.scoproject.pertahubagent.ui.common.BaseActivity
import com.scoproject.pertahubagent.vo.ProductVo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_list.*
import kotlinx.android.synthetic.main.item_sell_product.view.*
import kotlinx.android.synthetic.main.toolbar_common.*
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 20/04/18.
 * Android Engineer
 * SCO Project
 */
class ProductListActivity : BaseActivity(), ProductListContract.View {
    companion object {
        const val TAG_CATEGORY_NAME = "category_name"
        const val TAG_CATEGORY_ID = "category_id"
    }
    @Inject
    lateinit var mProductListPresenter : ProductListPresenter



    override fun onActivityReady(savedInstanceState: Bundle?) {
        mProductListPresenter.attachView(this)
        setupUIListener()
        val bundle = intent.extras
        if (bundle != null) {
            val categoryId = bundle.getString(TAG_CATEGORY_ID)
            tvTitleToolbar.text = "Daftar Produk"
            mProductListPresenter.getProductList(categoryId)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_product_list
    }

    override fun setAdapterProductList(data: List<ProductVo>) {
        rvProductList.setUp(data, R.layout.item_sell_product, {
            tvProductName.text = it.categoryName
            Picasso.with(context).load(it.imageSrc).into(ivProduct)
        },{ data -> mActivityNavigation.navigateToProductDetailPage(data) }, GridLayoutManager(this,2))
    }

    override fun setupUIListener() {
        ivBackBtn.setOnClickListener { this.finish() }
    }

}