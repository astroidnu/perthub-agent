package com.scoproject.pertahubagent.ui.common.navigationcontroller

import android.support.v7.app.AppCompatActivity
import com.scoproject.pertahubagent.ui.activity.dashboard.DashboardActivity
import com.scoproject.pertahubagent.ui.activity.login.LoginActivity
import com.scoproject.pertahubagent.ui.activity.productdetail.ProductDetailActivity
import com.scoproject.pertahubagent.ui.activity.productlist.ProductListActivity
import com.scoproject.pertahubagent.ui.activity.register.RegisterActivity
import com.scoproject.pertahubagent.ui.activity.responseorderpage.ResponseOrderPageActivity
import com.scoproject.pertahubagent.vo.ProductVo

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
class ActivityNavigation(val activity: AppCompatActivity) : BaseActivityNavigation() {
    /**
     * Navigate to Registration Page
     * */

    fun navigateToRegisterPage(){
        val registerIntent = newIntent(activity, RegisterActivity::class.java)
        activity.startActivity(registerIntent)
    }

    /**
     * Navigate to Dashboard Page
     * */

    fun navigateToDashboardPage(){
        val dashboardIntent = newIntent(activity, DashboardActivity::class.java)
        activity.startActivity(dashboardIntent)
    }


    fun navigateToLogin(){
        val intent = newIntent(activity, LoginActivity::class.java)
        activity.startActivity(intent)
    }

    fun navigateToResponseOrderPage() {
        val intent = newIntent(activity, ResponseOrderPageActivity::class.java)
        activity.startActivity(intent)
    }

    /**
     * Navigate to Product List Page
     * */

    fun navigateToProductList(categoryName :String, categoryId :String){
        val intent = newIntent(activity, ProductListActivity::class.java)
        intent.apply {
            putExtra(ProductListActivity.TAG_CATEGORY_ID, categoryId)
            putExtra(ProductListActivity.TAG_CATEGORY_NAME, categoryName)
        }
        activity.startActivity(intent)
    }

    /**
     * Navigate to Product Detail Page
     * */

    fun navigateToProductDetailPage(data : ProductVo){
        val intent = newIntent(activity, ProductDetailActivity::class.java)
        intent.apply {
            putExtra(ProductDetailActivity.TAG_DATA, data)
        }
        activity.startActivity(intent)
    }


}