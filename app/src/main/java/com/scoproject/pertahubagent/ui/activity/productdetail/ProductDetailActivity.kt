package com.scoproject.pertahubagent.ui.activity.productdetail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.BaseActivity
import com.scoproject.pertahubagent.vo.ProductVo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.toolbar_common.*
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 20/04/18.
 * Android Engineer
 * SCO Project
 */
class ProductDetailActivity : BaseActivity(), ProductDetailContract.View {
    companion object {
        const val TAG_DATA = "data"
    }

    @Inject
    lateinit var mProductDetailPresenter: ProductDetailPresenter

    @Inject
    lateinit var mLoginSession: LoginSession

    lateinit var mData : ProductVo

    var mProductId : String? = null

    override fun onActivityReady(savedInstanceState: Bundle?) {
        mProductDetailPresenter.attachView(this)
        val bundle = intent.extras
        if (bundle != null) {
            mData = bundle.getParcelable(TAG_DATA) as ProductVo
            tvTitleToolbar.text = "Detail Produk"
            mData.id?.let {
                mProductId = it
                mProductDetailPresenter.getProduct(it)
                setupUIListener()
            }

            if(mLoginSession.getUserGroupId() == 9){
                llMyStock.visibility = View.GONE
                tvLabelStockProduct.text = "Stock Produk Saya"
            }else{
                llMyStock.visibility = View.VISIBLE
            }
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_product_detail
    }

    override fun setContent(productName: String, productStock: String, myProductStock :String?) {
        tvProductDetailName.text = productName
        tvProductDetailStock.text = productStock
        tvProductDetailMyStock.text = myProductStock
        Picasso.with(this).load(mData.imageSrc).into(ivProductDetail)
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun navigateToSuccessOrderPage() {
        mActivityNavigation.navigateToResponseOrderPage()
    }

    override fun setupUIListener() {
        btnProductDetailSubmitOrder.setOnClickListener {
           mProductDetailPresenter.submitOrder(etOrderAmount.text.toString(),mProductId)
        }
        ivBackBtn.setOnClickListener { this.finish() }
    }




}