package com.scoproject.pertahubagent.ui.activity.login

import com.scoproject.pertahubagent.ui.base.BaseView

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
class LoginContract{
    interface View :BaseView{
        fun setupUIListener()
        fun showLoading()
        fun hideLoading()
        fun navigateToDashboard()
        fun showError(error:String)
    }

    interface UserActionListener {
        fun doLogin(username:String, password:String, fcmToken :String)
        fun checkLogin()
    }
}