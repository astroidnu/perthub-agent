package com.scoproject.pertahubagent.ui.activity.productdetail

import com.scoproject.pertahubagent.ui.base.BaseView

/**
 * Created by ibnumuzzakkir on 20/04/18.
 * Android Engineer
 * SCO Project
 */
class ProductDetailContract {
    interface View : BaseView {
        fun setContent(productName: String, productStock: String, myProductStock :String?)
        fun showLoading()
        fun hideLoading()
        fun showToast(msg : String)
        fun navigateToSuccessOrderPage()
        fun setupUIListener()
    }
    interface UserActionListener {
        fun getProduct(productId :String)
        fun submitOrder(amountOrder : String?, productId: String?)
    }
}