package com.scoproject.pertahubagent.ui.activity.register

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.ui.common.BaseActivity
import kotlinx.android.synthetic.main.activity_registration.*
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 12/04/18.
 * Android Engineer
 * SCO Project
 */
class RegisterActivity :BaseActivity(), RegisterContract.View,
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{
    @Inject
    lateinit var mRegisterPresenter: RegisterPresenter

    private val REQUEST_PLACE_PICKER = 100

    private var mGoogleApiClient: GoogleApiClient? = null
    private var mMap: GoogleMap? = null
    private var mapFrag: SupportMapFragment? = null
    private var latLng : LatLng? = null
    private var currLocationMarker : Marker? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_registration
    }

    override fun onActivityReady(savedInstanceState: Bundle?) {
        mRegisterPresenter.attachView(this)
        mapFrag = supportFragmentManager.findFragmentById(R.id.registrationMap) as SupportMapFragment
        mapFrag!!.getMapAsync(this)

//        rl_search_gps .setOnClickListener {
//            try {
//                val intentBuilder = PlacePicker.IntentBuilder()
//                val intent = intentBuilder.build(this)
//                // Start the intent by requesting a result,
//                // identified by a request code.
//                startActivityForResult(intent, REQUEST_PLACE_PICKER)
//
//            } catch (e : GooglePlayServicesRepairableException) {
//                Log.e(javaClass.name, e.message.toString())
//            } catch (e : GooglePlayServicesNotAvailableException) {
//                Log.e(javaClass.name, e.message.toString())
//            }
//        }

        btnRegistrationSubmit.setOnClickListener {

        }
    }

    private fun setupGoogleClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()

        if (mGoogleApiClient != null) {
            mGoogleApiClient?.connect()
        }
    }

    override fun onDestroy() {
        mGoogleApiClient?.disconnect()
        super.onDestroy()
    }

    /**
     * Callback google map
     * */


    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        mMap?.uiSettings?.isScrollGesturesEnabled = false
        mMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN
        setupGoogleClient()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        //do nothing
    }

    override fun onConnected(p0: Bundle?) {
        mMap?.clear()
        latLng = LatLng(-6.175372,106.827149)
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15F))

    }

    override fun onConnectionSuspended(p0: Int) {}


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == REQUEST_PLACE_PICKER && resultCode == Activity.RESULT_OK){
            // The user has selected a place. Extract the name and address.
            mMap!!.clear()
//            val place = PlacePicker.getPlace(this, data)
//            val latLng = place.latLng
//
//            var markerOptions = MarkerOptions()
//            markerOptions.position(latLng)
//
//            mRegisterPresenter.mLatLng = latLng
//
//            //Setup Marker
//            val bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.ic_marker_home)
//            val markerHome = BitmapDescriptorFactory.fromBitmap(bitmap)
//            markerOptions.icon(markerHome)
//            mMap!!.addMarker(markerOptions)
//            mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15F))
        }else{
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    
}