package com.scoproject.pertahubagent.ui.activity.login

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.iid.FirebaseInstanceId
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.BaseActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 12/04/18.
 * Android Engineer
 * SCO Project
 */
class LoginActivity : BaseActivity(), LoginContract.View{
    @Inject
    lateinit var mLoginPresenter : LoginPresenter

    @Inject
    lateinit var mLoginSession : LoginSession

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }

    override fun onActivityReady(savedInstanceState: Bundle?) {
        mLoginPresenter.attachView(this)
        mLoginPresenter.checkLogin()
        setupUIListener()
        val fbToken =  FirebaseInstanceId.getInstance().token
        fbToken?.let {
            mLoginSession.saveFirebaseToken(it)
        }
    }

    override fun setupUIListener() {
        tvLoginRegistrationLink.setOnClickListener {
            mActivityNavigation.navigateToRegisterPage()

        }
        btnLoginSubmit.setOnClickListener {
            val mPhoneNumber = etPhoneNumber.text.toString()
            val mPassword = etPassword.text.toString()
            mLoginPresenter.doLogin(mPhoneNumber,mPassword, mLoginSession.getFirebaseToken())
        }
    }

    override fun showLoading() {
        pbLogin.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pbLogin.visibility = View.GONE
    }

    override fun navigateToDashboard() {
        this.finish()
        mActivityNavigation.navigateToDashboardPage()
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onStop() {
        super.onStop()
        mLoginPresenter.detachView()
    }

}