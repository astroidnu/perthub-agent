package com.scoproject.pertahubagent.ui.activity.responseorderpage

import com.scoproject.pertahubagent.ui.base.BaseView

/**
 * Created by ibnumuzzakkir on 21/04/18.
 * Android Engineer
 * SCO Project
 */
class ResponseOrderPageContract {
    interface View : BaseView {
        fun setupUIListener()

    }
    interface UserActionListener {

    }
}