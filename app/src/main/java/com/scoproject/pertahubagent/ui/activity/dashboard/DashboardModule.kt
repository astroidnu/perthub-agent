package com.scoproject.pertahubagent.ui.activity.dashboard

import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.di.scope.ActivityScope
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.navigationcontroller.ActivityNavigation
import com.scoproject.pertahubagent.ui.common.navigationcontroller.FragmentNavigation
import com.scoproject.pertahubagent.ui.fragment.history.HistoryPresenter
import com.scoproject.pertahubagent.ui.fragment.history.completed.CompletedFragment
import com.scoproject.pertahubagent.ui.fragment.history.completed.CompletedPresenter
import com.scoproject.pertahubagent.ui.fragment.history.inprocess.InProcessPresenter
import com.scoproject.pertahubagent.ui.fragment.history.waitingforapprove.WaitingForApprovePresenter
import com.scoproject.pertahubagent.ui.fragment.home.HomePresenter
import com.scoproject.pertahubagent.ui.fragment.profile.ProfilePresenter
import com.scoproject.pertahubagent.utils.AppSchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
@Module
class DashboardModule {
    @Provides
    @ActivityScope
    internal fun provideDashboardActivity(dashboardActivity: DashboardActivity): ActivityNavigation {
        return ActivityNavigation(dashboardActivity)
    }

    @Provides @ActivityScope
    internal fun provideFragmentNavigation(dashboardActivity: DashboardActivity): FragmentNavigation {
        return FragmentNavigation(dashboardActivity, R.id.frameDashboard)
    }

    /**
     * ==============
     * This is section for provide alleny fragment in dashboard
     * ==============
     * */

    @Provides @ActivityScope
    fun provideHistoryFragment(compositeDisposable: CompositeDisposable,
                               schedulerProvider: AppSchedulerProvider) : HistoryPresenter {
        return HistoryPresenter(compositeDisposable,schedulerProvider)
    }

    @Provides @ActivityScope
    fun provideHomeFragment(networkService: NetworkService,
                            loginSession: LoginSession,
                            compositeDisposable: CompositeDisposable,
                            schedulerProvider: AppSchedulerProvider) : HomePresenter {
        return HomePresenter(networkService,loginSession,compositeDisposable,schedulerProvider)
    }

    @Provides @ActivityScope
    fun provideProfileFragment(networkService: NetworkService,
                               loginSession: LoginSession,
                               compositeDisposable: CompositeDisposable,
                               schedulerProvider: AppSchedulerProvider) : ProfilePresenter {
        return ProfilePresenter(networkService,loginSession,compositeDisposable,schedulerProvider)
    }


    @Provides @ActivityScope
    fun provideCompletedFragment(networkService: NetworkService,
                                 loginSession: LoginSession,
                                 compositeDisposable: CompositeDisposable,
                            schedulerProvider: AppSchedulerProvider) : CompletedPresenter {
        return CompletedPresenter(networkService,loginSession,compositeDisposable,schedulerProvider)
    }

    @Provides @ActivityScope
    fun provideInProcessFragment(networkService: NetworkService,
                                 loginSession: LoginSession,
                                 compositeDisposable: CompositeDisposable,
                               schedulerProvider: AppSchedulerProvider) : InProcessPresenter {
        return InProcessPresenter(networkService,loginSession,compositeDisposable,schedulerProvider)
    }

    @Provides @ActivityScope
    fun provideWaitingApproveFragment(networkService: NetworkService,
                                 loginSession: LoginSession,
                                 compositeDisposable: CompositeDisposable,
                                 schedulerProvider: AppSchedulerProvider) : WaitingForApprovePresenter {
        return WaitingForApprovePresenter(networkService,loginSession,compositeDisposable,schedulerProvider)
    }
}