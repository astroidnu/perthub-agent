package com.scoproject.pertahubagent.ui.activity.productlist

import com.scoproject.pertahubagent.ui.base.BaseView
import com.scoproject.pertahubagent.vo.ProductVo

/**
 * Created by ibnumuzzakkir on 20/04/18.
 * Android Engineer
 * SCO Project
 */
class ProductListContract {
    interface View : BaseView {
        fun setupUIListener()
        fun setAdapterProductList(data : List<ProductVo>)

    }
    interface UserActionListener {
        fun getProductList(categoryId :String)
    }
}