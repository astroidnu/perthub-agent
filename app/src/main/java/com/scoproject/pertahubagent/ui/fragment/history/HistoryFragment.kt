package com.scoproject.pertahubagent.ui.fragment.history

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.activity.dashboard.DashboardActivity
import com.scoproject.pertahubagent.ui.common.BaseFragment
import com.scoproject.pertahubagent.ui.fragment.history.completed.CompletedFragment
import com.scoproject.pertahubagent.ui.fragment.history.inprocess.InProcessFragment
import com.scoproject.pertahubagent.ui.fragment.history.waitingforapprove.WaitingForApproveFragment
import kotlinx.android.synthetic.main.fragment_history.*
import javax.inject.Inject


/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
class HistoryFragment : BaseFragment(), HistoryContract.View{
    @Inject
    lateinit var mHistoryPresenter: HistoryPresenter

    @Inject
    lateinit var mLoginSession : LoginSession

    override fun getLayoutId(): Int {
        return R.layout.fragment_history
    }

    override fun onLoadFragment(saveInstance: Bundle?) {
        mHistoryPresenter.attachView(this)
        setupUI()
        setupViewPager(vpHistory)
        tabLayoutHistory.setupWithViewPager(vpHistory)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(childFragmentManager)
        if(mLoginSession.getUserGroupId() == 9){
            adapter.addFragment(WaitingForApproveFragment(), "Permintaan")
        }
        adapter.addFragment(InProcessFragment(), "Proses")
        adapter.addFragment(CompletedFragment(), "Selesai")
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 3

    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    override fun onStop() {
        super.onStop()
        mHistoryPresenter.detachView()
    }

    override fun setupUI() {
        if(activity is DashboardActivity){
            (activity as DashboardActivity).showToolbar()
        }
    }

}