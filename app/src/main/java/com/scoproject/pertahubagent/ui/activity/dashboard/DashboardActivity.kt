package com.scoproject.pertahubagent.ui.activity.dashboard

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.ui.common.BaseActivity
import com.scoproject.pertahubagent.ui.common.navigationcontroller.FragmentNavigation
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_dashboard.*
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 12/04/18.
 * Android Engineer
 * SCO Project
 */
class DashboardActivity : BaseActivity(), DashboardContract.View, HasSupportFragmentInjector {
    @Inject
    lateinit var mFragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var mFragmentNavigation:FragmentNavigation

    override fun onActivityReady(savedInstanceState: Bundle?) {
        setupBottomNavigation()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_dashboard
    }

    override fun setupBottomNavigation() {
        bottomBarDashboard.setOnTabSelectListener { tabId ->
            when(tabId) {
                R.id.navigation_item_home ->  mFragmentNavigation.navigateToHomePage()
                R.id.navigation_item_riwayat -> mFragmentNavigation.navigateToHistoryPage()
                R.id.navigation_item_profile -> mFragmentNavigation.navigateToProfilePage()
                else ->  mFragmentNavigation.navigateToHomePage()
            }
        }
    }


    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return mFragmentDispatchingAndroidInjector
    }

    override fun hideToolbar() {
        toolbarDashboard.visibility = View.GONE
    }

    override fun showToolbar() {
        toolbarDashboard.visibility = View.VISIBLE
    }




}