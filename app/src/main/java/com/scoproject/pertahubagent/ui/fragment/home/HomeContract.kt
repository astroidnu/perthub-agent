package com.scoproject.pertahubagent.ui.fragment.home

import com.scoproject.pertahubagent.ui.base.BaseView
import com.scoproject.pertahubagent.vo.CategoryVo

/**
 * Created by ibnumuzzakkir on 15/04/18.
 * Android Engineer
 * SCO Project
 */
class HomeContract {
    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun showError(error :String)
        fun setUpProductList(data : List<CategoryVo>)
        fun setupUI()
    }

    interface UserActionListener {
        fun getProduct()

    }
}