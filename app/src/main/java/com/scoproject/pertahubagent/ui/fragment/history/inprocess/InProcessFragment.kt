package com.scoproject.pertahubagent.ui.fragment.history.inprocess

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.adapter.setUp
import com.scoproject.pertahubagent.ui.common.BaseFragment
import com.scoproject.pertahubagent.ui.fragment.history.HistoryContract
import com.scoproject.pertahubagent.utils.AppConstants
import com.scoproject.pertahubagent.utils.Helper
import com.scoproject.pertahubagent.vo.HistoryOrderVo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_inprocess_completed.*
import kotlinx.android.synthetic.main.item_history.view.*
import kotlinx.android.synthetic.main.item_history_content.view.*
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 16/04/18.
 * Android Engineer
 * SCO Project
 */
class InProcessFragment : BaseFragment(), InProcessContract.View{
    @Inject
    lateinit var mInProcessPresenter: InProcessPresenter

    @Inject
    lateinit var mHelper : Helper

    override fun getLayoutId(): Int {
        return R.layout.fragment_inprocess_completed
    }

    override fun onLoadFragment(saveInstance: Bundle?) {
        mInProcessPresenter.attachView(this)
        mInProcessPresenter.getHistoryOrder()
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showToast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun setAdapter(data: List<HistoryOrderVo>) {
        rvInProcessCompleted.setUp(data, R.layout.item_history, {
            tvOrderId.text = it.orderId
            tvOrderAmount.text = it.qty
            tvOrderProductName.text = it.productName
            llHistorySendContent.setBackgroundColor(resources.getColor(mHelper.getOrderColorCode(it.status)))
            tvOrderStatus.text = when(it.status){
                0 -> AppConstants.ORDER_STATUS.REQUESTED
                1 -> AppConstants.ORDER_STATUS.APPROVED
                2 -> AppConstants.ORDER_STATUS.CANCEL
                3 -> AppConstants.ORDER_STATUS.DONE
                else -> {
                    "-"
                }
            }
        },{ _ -> }, LinearLayoutManager(context))
    }

    override fun showNoHistory() {
        tvInProcessCompleteNoData.visibility = View.VISIBLE
        rvInProcessCompleted.visibility = View.GONE
    }

    override fun hideNoHistory() {
        tvInProcessCompleteNoData.visibility = View.GONE
        rvInProcessCompleted.visibility = View.VISIBLE
    }

    override fun onStop() {
        super.onStop()
        mInProcessPresenter.detachView()
    }


}
