package com.scoproject.pertahubagent.ui.activity.register

import com.scoproject.pertahubagent.ui.base.BaseView

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
class RegisterContract {
    interface View : BaseView{

    }

    interface UserActionListener {
    }
}