package com.scoproject.pertahubagent.ui.activity.responseorderpage

import com.google.gson.Gson
import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.activity.productlist.ProductListContract
import com.scoproject.pertahubagent.ui.common.BasePresenter
import com.scoproject.pertahubagent.utils.Helper
import com.scoproject.pertahubagent.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 21/04/18.
 * Android Engineer
 * SCO Project
 */
class ResponseOrderPagePresenter @Inject constructor(
        disposable : CompositeDisposable,
        scheduler : SchedulerProvider
):  BasePresenter<ResponseOrderPageContract.View>(disposable,scheduler), ResponseOrderPageContract.UserActionListener {

}