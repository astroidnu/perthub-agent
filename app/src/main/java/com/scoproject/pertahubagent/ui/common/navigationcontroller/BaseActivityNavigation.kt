package com.scoproject.pertahubagent.ui.common.navigationcontroller

import android.content.Context
import android.content.Intent
import android.net.Uri

/**
 * Created by ibnumuzzakkir on 12/04/18.
 * Android Engineer
 * SCO Project
 */
open class BaseActivityNavigation{
    /**
     * Intent Common Function
     * Handling new intent
     * */

    fun <T> newIntent(context: Context, cls: Class<T>): Intent {
        return Intent(context, cls)
    }

    fun newIntentUri(label: String, uri: Uri): Intent {
        return Intent(label, uri)
    }
}