package com.scoproject.pertahubagent.ui.fragment.history.waitingforapprove

import com.scoproject.pertahubagent.ui.base.BaseView
import com.scoproject.pertahubagent.vo.HistoryOrderVo

/**
 * Created by ibnumuzzakkir on 22/04/18.
 * Android Engineer
 * SCO Project
 */
class WaitingForApproveContract {
    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun showNoHistory()
        fun hideNoHistory()
        fun showToast(msg :String)
        fun setAdapter(data :List<HistoryOrderVo>)
        fun showAlertDialog(orderId : String)
        fun navigateToDashboard()
    }
    interface UserActionListener {
        fun getHistoryOrder()
        fun approveOrderStatus(orderId: String)
        fun cancelOrderStatus(orderId: String)
    }
}