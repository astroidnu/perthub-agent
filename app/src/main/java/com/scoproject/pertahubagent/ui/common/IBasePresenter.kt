package com.scoproject.pertahub.ui.base

import com.scoproject.pertahubagent.ui.base.BaseView

/**
 * Created by ibnumuzzakkir on 10/12/17.
 * Android Engineer
 * SCO Project
 */
interface IBasePresenter<in V : BaseView> {

    fun attachView(view: V)

    fun detachView()
}