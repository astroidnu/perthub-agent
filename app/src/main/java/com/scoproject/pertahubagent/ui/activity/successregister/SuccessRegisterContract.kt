package com.scoproject.pertahubagent.ui.activity.successregister

import com.scoproject.pertahubagent.ui.base.BaseView

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */
class SuccessRegisterContract {
    interface View : BaseView {

    }

    interface UserActionListener {

    }
}