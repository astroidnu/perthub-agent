package com.scoproject.pertahubagent.ui.activity.splashscreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.ui.activity.login.LoginActivity

/**
 * Created by ibnumuzzakkir on 12/04/18.
 * Android Engineer
 * SCO Project
 */
class SplashActivity : AppCompatActivity(){
    private val SPLASH_TIMEOUT = 1000 // 1 seconds timeout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        runSplashScreen(SPLASH_TIMEOUT.toLong())
    }

    /**
     * Run Splash Screen
     * */

    fun runSplashScreen(timeout: Long) {
        Handler().postDelayed({
            kotlin.run {
                var intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }, timeout)
    }

}