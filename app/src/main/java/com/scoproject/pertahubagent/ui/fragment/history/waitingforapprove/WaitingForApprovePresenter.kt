package com.scoproject.pertahubagent.ui.fragment.history.waitingforapprove

import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.BasePresenter
import com.scoproject.pertahubagent.utils.SchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 22/04/18.
 * Android Engineer
 * SCO Project
 */
class WaitingForApprovePresenter @Inject constructor(private val mNetworkService: NetworkService,
                                                     private val mLoginSession: LoginSession,
                                                     disposable : CompositeDisposable,
                                                     scheduler : SchedulerProvider
):  BasePresenter<WaitingForApproveContract.View>(disposable,scheduler), WaitingForApproveContract.UserActionListener {
    override fun getHistoryOrder() {
        view?.showLoading()
        disposable.add(
                mNetworkService.getAllOrdered("bearer " + mLoginSession.getLoginToken())
                        .subscribeOn(scheduler.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            result ->
                            view?.hideLoading()
                            val data = result.data.filter { it.status == 0 }.toList()
                            if(data.isNotEmpty()){
                                view?.hideNoHistory()
                                view?.setAdapter(data)
                            }else{
                                view?.showNoHistory()
                            }
                        },{ e ->
                            view?.hideLoading()
                        })
        )
    }

    override fun approveOrderStatus(orderId: String) {
        view?.showLoading()
        disposable.add(
                mNetworkService.approveOrder("bearer " + mLoginSession.getLoginToken(), orderId)
                        .subscribeOn(scheduler.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            result ->
                            view?.hideLoading()
                            view?.showToast(result.resultMessage)
                            view?.navigateToDashboard()
                        },{ e ->
                            view?.hideLoading()
                        })
        )
    }

    override fun cancelOrderStatus(orderId: String) {
        view?.showLoading()
        disposable.add(
                mNetworkService.cancelOrder("bearer " + mLoginSession.getLoginToken(), orderId)
                        .subscribeOn(scheduler.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            result ->
                            view?.hideLoading()
                            view?.showToast(result.resultMessage)
                            view?.navigateToDashboard()
                        },{ e ->
                            view?.hideLoading()
                        })
        )
    }

}