package com.scoproject.pertahubagent.ui.fragment.profile

import com.scoproject.pertahubagent.ui.base.BaseView

/**
 * Created by ibnumuzzakkir on 15/04/18.
 * Android Engineer
 * SCO Project
 */
class ProfileContract {
    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun showToast(msg :String)
        fun setContent(foto :String, ownerName : String, companyName :String)
        fun setupUI()
    }

    interface UserActionListener {
        fun getProfileData()
    }
}