package com.scoproject.pertahubagent.ui.fragment.profile

import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.BasePresenter
import com.scoproject.pertahubagent.utils.SchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 15/04/18.
 * Android Engineer
 * SCO Project
 */
class ProfilePresenter @Inject constructor(private val mNetworkService: NetworkService,
                                           private val mLoginSession: LoginSession,
                                           disposable : CompositeDisposable,
                                           scheduler : SchedulerProvider
):  BasePresenter<ProfileContract.View>(disposable,scheduler), ProfileContract.UserActionListener {

    override fun getProfileData() {
        view?.showLoading()
        disposable.add(
                mNetworkService.getProfile("bearer " + mLoginSession.getLoginToken())
                        .subscribeOn(scheduler.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            result ->
                            view?.hideLoading()
                            val data = result.data
                            view?.setContent(data.foto, data.name, mLoginSession.getUserGroupName())
                        },{ e ->
                            view?.hideLoading()
                            view?.showToast(e.message.toString())
                        })
        )
    }

}