package com.scoproject.pertahubagent.ui.common

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleRegistry
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.scoproject.pertahubagent.ui.base.BaseView
import com.scoproject.pertahubagent.ui.common.navigationcontroller.ActivityNavigation
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 13/04/18.
 * Android Engineer
 * SCO Project
 */
abstract class BaseFragment : Fragment(), BaseView{
    @Inject
    lateinit var mActivityNavigation: ActivityNavigation


    private val mLifecycleRegistry : LifecycleRegistry by lazy{
        LifecycleRegistry(this)
    }

    /**
     * Getting Layout ID from activity
     * */

    abstract fun getLayoutId () : Int

    /**
     * This method will be executed after view has been create in fragment
     */

    var mView : View? = null

    private var presenter: BasePresenter<*>? = null

    protected abstract fun onLoadFragment(saveInstance: Bundle?)

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        inflater.let {
            mView  = inflater.inflate(getLayoutId(), container, false)
        }
        return mView
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
        this.presenter = presenter
    }

    /**
     * This method will be executed after all views in fragment has rendered
     * */

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onLoadFragment(savedInstanceState)
    }

    fun getCurrentActivity() : FragmentActivity? {
        return activity
    }

    override fun getLifecycle(): Lifecycle {
        return mLifecycleRegistry
    }
}