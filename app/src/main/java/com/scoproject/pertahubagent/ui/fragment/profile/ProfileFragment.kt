package com.scoproject.pertahubagent.ui.fragment.profile

import android.os.Bundle
import android.widget.Toast
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.activity.dashboard.DashboardActivity
import com.scoproject.pertahubagent.ui.common.BaseFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.item_sell_product.view.*
import javax.inject.Inject


/**
 * Created by ibnumuzzakkir on 15/04/18.
 * Android Engineer
 * SCO Project
 */
class ProfileFragment : BaseFragment(), ProfileContract.View {
    @Inject
    lateinit var mProfilePresenter : ProfilePresenter

    @Inject
    lateinit var mLoginSession: LoginSession

    override fun getLayoutId(): Int {
        return R.layout.fragment_profile
    }

    override fun onLoadFragment(saveInstance: Bundle?) {
        mProfilePresenter.attachView(this)
        setupUI()
        mProfilePresenter.getProfileData()
        btnLogout.setOnClickListener {
            mLoginSession.clear()
            mActivityNavigation.navigateToLogin()
        }
    }


    override fun onStop() {
        super.onStop()
        mProfilePresenter.detachView()
    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun showToast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun setContent(foto: String, ownerName: String, companyName: String) {
        Picasso.with(context).load(foto).into(ivProfile)
        tvProfileCompanyName.text = companyName
        tvProfileOwnerName.text = ownerName
    }

    override fun setupUI() {
        if(activity is DashboardActivity){
            (activity as DashboardActivity).showToolbar()
        }
    }


}