package com.scoproject.pertahubagent.ui.activity.register

import com.google.android.gms.maps.model.LatLng
import com.scoproject.pertahubagent.ui.common.BasePresenter
import com.scoproject.pertahubagent.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
class RegisterPresenter  @Inject constructor(disposable : CompositeDisposable,
                                             scheduler : SchedulerProvider
):  BasePresenter<RegisterContract.View>(disposable,scheduler),RegisterContract.UserActionListener {

    lateinit var mLatLng : LatLng

}