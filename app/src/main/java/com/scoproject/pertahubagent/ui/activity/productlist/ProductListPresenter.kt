package com.scoproject.pertahubagent.ui.activity.productlist

import com.google.gson.Gson
import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.BasePresenter
import com.scoproject.pertahubagent.utils.Helper
import com.scoproject.pertahubagent.utils.SchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 20/04/18.
 * Android Engineer
 * SCO Project
 */
class ProductListPresenter @Inject constructor(private val mNetworkService: NetworkService,
                                               private val mHelper: Helper,
                                               private val mGson : Gson,
                                               private val mLoginSession : LoginSession,
                                               disposable : CompositeDisposable,
                                               scheduler : SchedulerProvider
):  BasePresenter<ProductListContract.View>(disposable,scheduler),ProductListContract.UserActionListener {

    override fun getProductList(categoryId :String) {
        disposable.add(
                mNetworkService.getProductByCategoryId("bearer " + mLoginSession.getLoginToken(),categoryId)
                        .subscribeOn(scheduler.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            result ->
                            view?.setAdapterProductList(result.data)
                        },{ e ->

                        })
        )
    }

}