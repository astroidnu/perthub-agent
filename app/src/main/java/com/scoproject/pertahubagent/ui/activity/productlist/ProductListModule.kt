package com.scoproject.pertahubagent.ui.activity.productlist

import com.google.gson.Gson
import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.di.scope.ActivityScope
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.navigationcontroller.ActivityNavigation
import com.scoproject.pertahubagent.utils.AppSchedulerProvider
import com.scoproject.pertahubagent.utils.Helper
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by ibnumuzzakkir on 20/04/18.
 * Android Engineer
 * SCO Project
 */
@Module
class ProductListModule {
    @Provides
    @ActivityScope
    internal fun provideProductListActivity(activity: ProductListActivity): ProductListContract.View {
        return activity
    }

    @Provides
    @ActivityScope
    internal fun provideActivityNavigation(productListActivity: ProductListActivity): ActivityNavigation {
        return ActivityNavigation(productListActivity)
    }

    @Provides
    @ActivityScope
    internal fun provideProductListPresenter(networkService: NetworkService,
                                       helper: Helper,
                                       gson : Gson,
                                       loginSession : LoginSession,
                                       compositeDisposable: CompositeDisposable,
                                       schedulerProvider: AppSchedulerProvider) : ProductListPresenter{
        return ProductListPresenter(networkService,helper,gson,loginSession,compositeDisposable, schedulerProvider)
    }

}