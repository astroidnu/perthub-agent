package com.scoproject.pertahubagent.ui.activity.register

import com.scoproject.pertahubagent.di.scope.ActivityScope
import com.scoproject.pertahubagent.ui.common.navigationcontroller.ActivityNavigation
import com.scoproject.pertahubagent.utils.AppSchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
@Module
class RegisterModule {
    @Provides
    @ActivityScope
    internal fun provideRegisterActivity(activity: RegisterActivity): RegisterContract.View {
        return activity
    }

    @Provides
    @ActivityScope
    internal fun provideActivityNavigation(activity: RegisterActivity): ActivityNavigation{
        return ActivityNavigation(activity)
    }

    @Provides
    @ActivityScope
    internal fun provideRegisterModule(compositeDisposable: CompositeDisposable,
                                       schedulerProvider: AppSchedulerProvider) : RegisterPresenter{
        return RegisterPresenter(compositeDisposable, schedulerProvider)
    }


}