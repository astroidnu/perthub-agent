package com.scoproject.pertahubagent.ui.activity.successregister

import android.os.Bundle
import com.scoproject.pertahubagent.R
import com.scoproject.pertahubagent.ui.common.BaseActivity

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */
class SuccessRegisterActivity : BaseActivity() {
    override fun onActivityReady(savedInstanceState: Bundle?) {

    }

    override fun getLayoutId(): Int {
        return R.layout.activity_success_register
    }

}