package com.scoproject.pertahubagent.ui.fragment.history

import com.scoproject.pertahubagent.ui.common.BasePresenter
import com.scoproject.pertahubagent.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */

class HistoryPresenter @Inject constructor(disposable : CompositeDisposable,
                                           scheduler : SchedulerProvider
):  BasePresenter<HistoryContract.View>(disposable,scheduler), HistoryContract.UserActionListener {

}
