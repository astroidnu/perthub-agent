package com.scoproject.pertahubagent.ui.activity.login

import com.google.gson.Gson
import com.scoproject.pertahubagent.api.NetworkService
import com.scoproject.pertahubagent.di.scope.ActivityScope
import com.scoproject.pertahubagent.session.LoginSession
import com.scoproject.pertahubagent.ui.common.navigationcontroller.ActivityNavigation
import com.scoproject.pertahubagent.utils.AppSchedulerProvider
import com.scoproject.pertahubagent.utils.Helper
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by ibnumuzzakkir on 14/04/18.
 * Android Engineer
 * SCO Project
 */
@Module
class LoginModule{
    @Provides
    @ActivityScope
    internal fun provideLoginActivity(activity: LoginActivity): LoginContract.View {
        return activity
    }

    @Provides
    @ActivityScope
    internal fun provideActivityNavigation(loginActivity: LoginActivity): ActivityNavigation{
        return ActivityNavigation(loginActivity)
    }

    @Provides
    @ActivityScope
    internal fun provideLoginPresenter(networkService: NetworkService,
                                       helper:Helper,
                                       gson : Gson,
                                       loginSession : LoginSession,
                                       compositeDisposable: CompositeDisposable,
                                      schedulerProvider: AppSchedulerProvider) : LoginPresenter{
        return LoginPresenter(networkService,helper,gson,loginSession,compositeDisposable, schedulerProvider)
    }



}