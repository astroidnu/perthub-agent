package com.scoproject.pertahubagent.api.response

import com.google.gson.annotations.SerializedName

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */
data class BaseApiResponse<T>(
        @SerializedName("responseCode") val resultCode : Int,
        @SerializedName("responseMessage") val resultMessage : String,
        @SerializedName("data") val data : T
)