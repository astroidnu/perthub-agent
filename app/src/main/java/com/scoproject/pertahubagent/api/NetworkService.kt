package com.scoproject.pertahubagent.api

import com.scoproject.pertahubagent.api.response.BaseApiResponse
import com.scoproject.pertahubagent.vo.*
import io.reactivex.Flowable
import retrofit2.http.*

/**
 * Created by ibnumuzzakkir on 19/04/18.
 * Android Engineer
 * SCO Project
 */
interface NetworkService {
    @FormUrlEncoded
    @POST("authenticate")
    fun login(@Field("username") username : String, @Field("password") password : String, @Field("fcm_token") fcmToken:String) : Flowable<BaseApiResponse<LoginVo>>

    @GET("get_category")
    fun getCategory(@Header("Authorization") authToken: String) : Flowable<BaseApiResponse<List<CategoryVo>>>

    @GET("profile")
    fun getProfile(@Header("Authorization") authToken: String) : Flowable<BaseApiResponse<ProfileVo>>

    @GET("detail_product/{productId}")
    fun getDetailProduct(@Header("Authorization") authToken: String) : Flowable<BaseApiResponse<List<ProductVo>>>

    @GET("get_order")
    fun getAllHistoryOrder(@Header("Authorization") authToken: String) : Flowable<BaseApiResponse<List<HistoryOrderVo>>>

    @FormUrlEncoded
    @POST("order")
    fun postOrder(@Header("Authorization") authToken: String,@FieldMap data : HashMap<String, String>) : Flowable<BaseApiResponse<Any>>

    @GET("get_stock/{productId}")
    fun getProductStock(@Header("Authorization") authToken: String, @Path("productId") productId : String) : Flowable<BaseApiResponse<List<ProductStockVo>>>


    @PUT("approve_order/{orderId}")
    fun approveOrder(@Header("Authorization") authToken: String, @Path("orderId") productId : String) : Flowable<BaseApiResponse<Any>>


    @PUT("cancel_ordered/{orderId}")
    fun cancelOrder(@Header("Authorization") authToken: String, @Path("orderId") productId : String) : Flowable<BaseApiResponse<Any>>


    @GET("get_ordered")
    fun getAllOrdered(@Header("Authorization") authToken: String) : Flowable<BaseApiResponse<List<HistoryOrderVo>>>

    @GET("get_product/{categoryId}")
    fun getProductByCategoryId(@Header("Authorization") authToken: String,@Path("categoryId") categoryId :String) : Flowable<BaseApiResponse<List<ProductVo>>>
}